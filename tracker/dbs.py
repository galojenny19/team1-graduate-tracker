from django.db import connections, connection
from .common import Repeated
from django.http import JsonResponse, HttpResponse 
from django.db import OperationalError
from rest_framework.response import Response
from rest_framework import status
from django.db import models
    
class DB():
    def __init__(self):
        pass
    
    def profiles(self, param):
        try:
            query = "CALL sp_" + param["SPName"] + "(@ReturnIsSuccess,\""+str(param)+"\")"
            print(Repeated.cleanup_query(query))     
            with connections['profiles'].cursor() as cursor:
                cursor.execute(Repeated.cleanup_query(query))   
                row = Repeated.dictfetchall(cursor)
                cursor.close()
                # connection.close()    
                return row
        except Exception as ex:
            return JsonResponse(ex, safe = False)         
        



