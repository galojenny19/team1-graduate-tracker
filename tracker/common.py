from django.shortcuts import redirect
from django.http import HttpResponse
from django.conf import settings
import urllib.request
import socket
import uuid
import sys, requests, urllib

class Repeated:
    apikey = settings.SEMAPHORE_KEY
    sender = settings.SEMAPHORE_SENDER
    
    def __init__(self):
        pass
            
    def send_message(self, data):
        print('Sending Message...') 
        params = (
            ('apikey', self.apikey),
            ('sendername', self.sender),
            ('message', data['message']),
            ('number', data['number'])
        )
        path = 'https://semaphore.co/api/v4/messages?' + urllib.parse.urlencode(params)
        result = requests.post(path)

        return 'Message Sent!'
     
    def dictfetchall(cursor):
        "Return all rows from a cursor as a dict"
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]  

    def cleanup_query(query): 
        try:
            # replace all single qoute delimiter to double
            query = query.replace("'", "\"")

            # replace first delimiter
            query = query.replace("\"", "'",1)

            # replace last delimiter
            s = query
            old = '"'
            new = '\''
            maxreplace = 1

            return new.join(s.rsplit(old, maxreplace))
                
        except:
            print("Unable to cleanup")                     


    def empty_to_null(value):
        return 'NULL' if value == None else value         

    def no_record():
        context = {
                 "IsSuccess" : 0
                ,"Result"  : 'No Record Found'
        }

        return context
    
    def form_method_invalid():
        context = {
                 "IsSuccess" : 0
                ,"Result"  : 'Form Method Invalid'
        }

        return context    
    
    def str_to_list(string):
        li = list(string.split(","))
        return li  
    
    def external_ip():
        external_ip = urllib.request.urlopen('https://ident.me').read().decode('utf8')
        return external_ip
    
    def get_local_hostname_ip(output):
        try:
            host_name = socket.gethostname()
            host_ip = socket.gethostbyname(host_name)

            if output == 'host':
                return host_name

            if output == 'ip':
                return host_ip
            
        except:
            return HttpResponse("Unable to get Hostname and IP")    

    def my_random_string(string_length=10):
        """Returns a random string of length string_length."""
        random = str(uuid.uuid4()) # Convert UUID format to a Python string.
        random = random.upper() # Make all characters uppercase.
        random = random.replace("-","") # Remove the UUID '-'.
        return random[0:string_length] # Return the random string.
    
    def key_not_exists(self, key, data):
        return True if key not in data or data[key] == '' else False
       


    
    