from django.urls import path
from tracker import views, students, external_links, courses, login

urlpatterns = [
    path('students/', views.StudentList.as_view()),
    path('students/v2/', students.Studentsv2.as_view()),
    path('courses/v2/', views.Coursesv2.as_view()),
    path('courses/', views.CourseLists.as_view()),
    path('profiles/', views.Profilesv2.as_view()),
    path('profiles/v2/', views.Profilesv2.as_view()),
    path('schools/', views.SchoolLists.as_view()),
    path('barangays/', views.BarangayLists.as_view()),
    path('cities/', views.CityLists.as_view()),
    path('provinces/', views.ProvinceLists.as_view()),
    path('schools/<int:pk>', views.SchoolDetail.as_view()),
    path('barangays/<int:pk>', views.BarangayDetail.as_view()),
    path('cities/<int:pk>', views.CityDetail.as_view()),
    path('school_transaction/', views.school_transaction, name="school_transaction"),
    path('barangay_transaction/', views.barangay_transaction, name="barangay_transaction"),
    path('city_transaction/', views.city_transaction, name="city_transaction"),
    path('province_transaction/', views.province_transaction, name="province_transaction"),
    path('profile_transaction/', views.profile_transaction, name="profile_transaction"),
    path('course_transaction/', external_links.course_transaction, name="course_transaction"),
    path('student_transaction/', views.student_transaction, name="student_transaction"),
    path('student_list/', external_links.student_list, name="student_list"),

]
