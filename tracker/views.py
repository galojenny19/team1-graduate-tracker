from .models import Courses, Students, StudentDetails, Profiles, Schools, Barangays, Cities
from .serializers import CourseSerializer, StudentSerializer, StudentDetailSerializer, SchoolAdminSerializer, ProfileSerializer, SchoolSerializer, BarangaySerializer, CitySerializer
from django.http import HttpResponse, JsonResponse
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated 
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework import status
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.http import Http404
from django.db import IntegrityError
from django.db import transaction
from tracker.common import Repeated 
from tracker.dbs import DB
from django.db import connections, connection    
from django.views.generic import (View,TemplateView)            

import requests
import json

    
class ProvinceLists(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)    
    # queryset = Cities.objects.all()
    # serializer_class = CitySerializer
    def get(self, request, format=None):
        db = DB()                
        context = {}
        list_data={}

        data = json.dumps(request.POST)
        context = json.loads(data)
        context['CountryID'] = 191 #philippines
        context['Flag'] = 1
        context['SPName'] = 'provinces'      
 
        list_data = db.profiles(context)
        connection.close() 
        return Response(list_data, status=status.HTTP_201_CREATED)    

class CityLists(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)    
    # queryset = Cities.objects.all()
    # serializer_class = CitySerializer
    def get(self, request, format=None):
        db = DB()                
        context = {}
        list_data={}

        province_id = request.GET.get('province_id', '')  
        data = json.dumps(request.POST)
        context = json.loads(data)
        context['ProvinceID'] = province_id
        context['Flag'] = 1
        context['SPName'] = 'city_municipality'      
 
        list_data = db.profiles(context)
        connection.close() 
        return Response(list_data, status=status.HTTP_201_CREATED)                     
    
class BarangayLists(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)    
    
    def get(self, request, format=None):
        # city_id = request.GET.get('city_id', '')  
        # if city_fk:
        #     Barangay = Barangays.objects.filter(city_fk=city_fk)    
        # else:
        #     Barangay = Barangays.objects.all()

        # serializer = BarangaySerializer(Barangay, many=True)
        # return Response(serializer.data)
        db = DB()                
        context = {}
        list_data={}

        city_id = request.GET.get('city_id', '')  
        data = json.dumps(request.POST)
        context = json.loads(data)
        context['CityMunicipalityID'] = city_id
        context['Flag'] = 1
        context['SPName'] = 'barangays'      
        print(context)
        list_data = db.profiles(context)
        connection.close() 
        return Response(list_data, status=status.HTTP_201_CREATED)        
    
    # def post(self, request, format=None):
    #     serializer = BarangaySerializer(data=request.data)
    #     if serializer.is_valid():
    #         serializer.save()
    #         return Response(serializer.data, status=status.HTTP_201_CREATED)
    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)     
    
class SchoolLists(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)    
    queryset = Schools.objects.all()
    serializer_class = SchoolSerializer    
    
class Coursesv2(generics.ListCreateAPIView):
    # permission_classes = (IsAuthenticated,)    
    # queryset = Courses.objects.all()
    # serializer_class = CourseSerializer        
    def get(self, request, format=None):
        course_name = request.GET.get('course_name', '')  
        if course_name:
            # user = CustomUser.object.get(email=email, password=password)
            # Course = CourseSerializer.objects.filter(course_id=course_id)    
            queryset = Courses.objects.filter(course_name__icontains=course_name)  
            
        else:
            queryset = Courses.objects.all()




        serializer = CourseSerializer(queryset, many=True)
        return Response(serializer.data)
        
    def post(self, request, format=None):    
      
        with transaction.atomic():        
            max_id = Courses.objects.last().course_id + 1
            course_data = request.POST.copy()
            course_data['course_code'] = Repeated.my_random_string(6) +'-'+ str(max_id)
            course = CourseSerializer(data=course_data)        
            if course.is_valid(raise_exception=True):   
                course.save()
                course_data['course_id'] = course.data['course_id']
                return Response(course_data, status=status.HTTP_201_CREATED)
            else:    
                transaction.set_rollback(True)  
                return Response(course.errors, status=status.HTTP_400_BAD_REQUEST)
    
class CourseLists(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)    
    queryset = Courses.objects.all()
    serializer_class = CourseSerializer        
                                            

class CourseDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Courses.objects.all()
    serializer_class = CourseSerializer

class Profilesv2(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)    
    
    def get(self, request, format=None):
        profile_id = request.GET.get('profile_id', '')  
        if profile_id:
            Profile = Profiles.objects.filter(profile_id=profile_id)    
        else:
            Profile = Profiles.objects.all()

        serializer = ProfileSerializer(Profile, many=True)
        return Response(serializer.data)
    
    def post(self, request, format=None):
        with transaction.atomic():
            db = DB()                
            value = Repeated()                
            context = {}
            list_data={}
            result_data={}

            data = json.dumps(request.POST)
            context = json.loads(data)

            school_serialize = SchoolSerializer(data=request.data)
            # school_admin_serialize = SchoolAdminSerializer(data=request.data)

            if school_serialize.is_valid(raise_exception=True) :
                
                if value.key_not_exists("barangay_fk_id", context):
                    return Response({"result":"barangay_fk_id is required"}, status=status.HTTP_201_CREATED)
                            
                context['UserLoginName'] = context['school_email']
                context['Flag'] = 3
                context['TransactBy'] = 999999
                context['SPName'] = 'profile'                
                list_data = db.profiles(context)
                
                if list_data[0]['IsSuccess'] == '1':
                    school_data = request.POST.copy()
                    school_data['school_code'] = Repeated.my_random_string(6)
                    school = SchoolSerializer(data=school_data)
                    if school.is_valid(raise_exception=True):
                        school.save() 
                        school_admin_data = request.POST.copy()
                        school_admin_data['profile_fk_id'] = list_data[0]['ProfileID']
                        school_admin_data['contact_person_name'] = list_data[0]['ProfileFirstName'] +' '+list_data[0]['ProfileLastName']
                        school_admin_data['school_fk'] = school.data['school_id']   

                        school_admin = SchoolAdminSerializer(data=school_admin_data)
                        if school_admin.is_valid(raise_exception=True):   
                                school_admin.save()
                                result_data['profile_id'] = list_data[0]['ProfileID']
                                result_data['profile_firstname'] = list_data[0]['ProfileFirstName']
                                result_data['profile_lastname'] = list_data[0]['ProfileLastName']
                                result_data['profile_birthday'] = list_data[0]['ProfileBirthdate']
                                result_data['school_id'] = school.data['school_id']   
                                result_data['school_code'] = school.data['school_code']   
                                result_data['school_name'] = school.data['school_name']   
                                result_data['school_street'] = school.data['school_street']   
                                result_data['school_email'] = school.data['school_email']   
                                result_data['barangay_fk_id'] = school.data['barangay_fk_id']   

                                # connection.close()    
                                return Response(result_data, status=status.HTTP_201_CREATED)                                
                        else:    
                            transaction.set_rollback(True)  
                            return Response(school_admin.errors, status=status.HTTP_400_BAD_REQUEST)        
                    else:    
                        transaction.set_rollback(True) 
                        return Response(school.errors, status=status.HTTP_400_BAD_REQUEST)
                else:    
                    transaction.set_rollback(True) 
                    return Response(list_data, status=status.HTTP_400_BAD_REQUEST)                    
            else:    
                transaction.set_rollback(True) 
                return Response(school_serialize.errors, status=status.HTTP_400_BAD_REQUEST)                    
            
class ProfileLists(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)    
    
    def get(self, request, format=None):
        profile_id = request.GET.get('profile_id', '')  
        if profile_id:
            Profile = Profiles.objects.filter(profile_id=profile_id)    
        else:
            Profile = Profiles.objects.all()

        serializer = ProfileSerializer(Profile, many=True)
        return Response(serializer.data)
    
    def post(self, request, format=None):
        with transaction.atomic():
            db = DB()                
            value = Repeated()                
            context = {}
            list_data={}
            result_data={}

            data = json.dumps(request.POST)
            context = json.loads(data)

            # if value.key_not_exists("school_code", context):
            #     return Response({"result":"school_code is required"}, status=status.HTTP_201_CREATED) 
            if value.key_not_exists("school_name", context):
                return Response({"result":"school_name is required"}, status=status.HTTP_201_CREATED) 
            if value.key_not_exists("school_email", context):
                return Response({"result":"school_email is required"}, status=status.HTTP_201_CREATED)                         
            if value.key_not_exists("school_contact", context):
                return Response({"result":"school_contact is required"}, status=status.HTTP_201_CREATED)                                     
            if value.key_not_exists("barangay_fk_id", context):
                return Response({"result":"barangay_fk_id is required"}, status=status.HTTP_201_CREATED)                                                 
            
            context['UserLoginName'] = context['school_email']
            context['Flag'] = 3
            context['TransactBy'] = 999999
            context['SPName'] = 'profile' 
            
            list_data = db.profiles(context)

            if list_data[0]['IsSuccess'] == '0':
                transaction.set_rollback(True)        
                return Response(list_data, status=status.HTTP_201_CREATED)                
            
            
            school_data = request.POST.copy()
            school_data['school_code'] = Repeated.my_random_string(10)
            # school = SchoolSerializer(data=request.data)
            school = SchoolSerializer(data=school_data)
            if school.is_valid(raise_exception=True):
                school.save() 
                school_admin_data = request.POST.copy()
                school_admin_data['profile_fk_id'] = list_data[0]['ProfileID']
                school_admin_data['contact_person_name'] = list_data[0]['ProfileFirstName'] +' '+list_data[0]['ProfileLastName']
                school_admin_data['school_fk'] = school.data['school_id']   

                school_admin = SchoolAdminSerializer(data=school_admin_data)
                if school_admin.is_valid(raise_exception=True):   
                        school_admin.save()

                        result_data['profile_id'] = list_data[0]['ProfileID']
                        result_data['profile_firstname'] = list_data[0]['ProfileFirstName']
                        result_data['profile_lastname'] = list_data[0]['ProfileLastName']
                        result_data['profile_birthday'] = list_data[0]['ProfileBirthdate']
                        result_data['school_id'] = school.data['school_id']   
                        result_data['school_code'] = school.data['school_code']   
                        result_data['school_name'] = school.data['school_name']   
                        result_data['school_street'] = school.data['school_street']   
                        result_data['school_email'] = school.data['school_email']   
                        result_data['barangay_fk_id'] = school.data['barangay_fk_id']   

                        # connection.close()    
                        return Response(result_data, status=status.HTTP_201_CREATED)                                
                else:    
                    transaction.set_rollback(True)  
                    return Response(school_admin.errors, status=status.HTTP_400_BAD_REQUEST)        
            else:    
                transaction.set_rollback(True) 
                return Response(school.errors, status=status.HTTP_400_BAD_REQUEST)                          
            
class CityDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Cities.objects.all()
    serializer_class = CitySerializer
    
class BarangayDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Barangays.objects.all()
    serializer_class = BarangaySerializer
    
class SchoolDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Schools.objects.all()
    serializer_class = SchoolSerializer

class ProfileDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Profiles.objects.all()
    serializer_class = ProfileSerializer    


            
class StudentList(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)    
    # queryset = Students.objects.all()
    # serializer_class = StudentSerializer    
    
    def get(self, request, format=None):
        student_id = request.GET.get('student_id', '')  
        if student_id:
            Student = Students.objects.filter(student_id=student_id)    
        else:
            Student = Students.objects.all()

        student = StudentSerializer(Student, many=True)
        return Response(student.data)
    
    def post(self, request, format=None):
        profile = ProfileSerializer(data=request.data)        

        
        list_data = {}        
        with transaction.atomic():        
            # if profile.is_valid(raise_exception=True):   
            #     profile.save()
                
            #     profile_data = request.POST.copy()
            #     profile_data['profile_fk'] = profile.data['profile_id']  
            db = DB()                
            value = Repeated()                
            context = {}
            list_data={}
            result_data={}

            data = json.dumps(request.POST)
            context = json.loads(data)

            if value.key_not_exists("employee_status", context):
                return Response({"result":"employee_status is required"}, status=status.HTTP_201_CREATED) 
            if value.key_not_exists("course_fk", context):
                return Response({"result":"course_fk is required"}, status=status.HTTP_201_CREATED) 
            if value.key_not_exists("school_fk", context):
                return Response({"result":"school_fk is required"}, status=status.HTTP_201_CREATED)   
            if value.key_not_exists("school_year_graduated", context):
                return Response({"result":"school_year_graduated is required"}, status=status.HTTP_201_CREATED)               
                                  
            
            context['Flag'] = 4
            context['TransactBy'] = 999999
            context['SPName'] = 'profile' 
            
            list_data = db.profiles(context)

            if list_data[0]['IsSuccess'] == '0':
                transaction.set_rollback(True)        
                return Response(list_data, status=status.HTTP_201_CREATED)                
                        
            profile_data = request.POST.copy()
            profile_data['profile_fk_id'] = list_data[0]['ProfileID']       
            profile_data['student_name'] = list_data[0]['ProfileFirstName'] +' '+list_data[0]['ProfileLastName']
            student = StudentSerializer(data=profile_data)
            if student.is_valid(raise_exception=True):
                student.save()
                
                student_data = request.POST.copy()
                student_data['student_fk'] = student.data['student_id']                          
                student_detail = StudentDetailSerializer(data=student_data)
                if student_detail.is_valid(raise_exception=True):
                    student_detail.save()
                    
                    result_data['profile_id'] = list_data[0]['ProfileID']
                    result_data['profile_firstname'] = list_data[0]['ProfileFirstName']
                    result_data['profile_lastname'] = list_data[0]['ProfileLastName']
                    result_data['profile_birthday'] = list_data[0]['ProfileBirthdate']   
                    result_data['student_id'] = student.data['student_id']   
                    result_data['company_name'] = student.data['company_name']   
                    result_data['employee_status'] = student.data['employee_status']   
                    result_data['profile_fk_id'] = student.data['profile_fk_id']   
                
                    return Response(result_data, status=status.HTTP_201_CREATED)                                
                else:    
                    transaction.set_rollback(True)  
                    return Response(student_detail.errors, status=status.HTTP_400_BAD_REQUEST)                                                                
            else:    
                transaction.set_rollback(True)  
                return Response(student.errors, status=status.HTTP_400_BAD_REQUEST)                                        
            # else:    
            #     transaction.set_rollback(True)  
            #     return Response(profile.errors, status=status.HTTP_400_BAD_REQUEST)                    
            
@csrf_exempt
@api_view(['GET','PUT', 'POST', ])
def student_transaction(request):
    if request.is_secure():
        url = 'https://' + request.META['HTTP_HOST']
    else:
        url = 'http://' + request.META['HTTP_HOST']
        
    student_id = request.GET.get('student_id', '')        
    url = url + F'/api/students/?=student_id{student_id}'        
    headers = {'Authorization': 'Token ' + settings.TOKEN}
    
    if request.method == 'POST':
        result = requests.post(url, data=request.data, headers=headers)    
    elif request.method == 'PUT':    
        result = requests.put(url, data=request.data, headers=headers)    
    else:    
        result = requests.get(url, data=request.data, headers=headers)    
    
    response = HttpResponse(
        content=result.content,
        status=result.status_code,
        content_type=result.headers['Content-Type']
    )

    return response


@csrf_exempt
@api_view(['GET','PUT', 'POST', ])
def school_transaction(request):
    if request.is_secure():
        url = 'https://' + request.META['HTTP_HOST']
    else:
        url = 'http://' + request.META['HTTP_HOST']
        
    id = request.GET.get('id', '')        
    url = url + F'/api/schools/{id}'        
    headers = {'Authorization': 'Token ' + settings.TOKEN}
    
    if request.method == 'POST' and request.POST:
        result = requests.post(url, data=request.data, headers=headers)    
    elif request.method == 'PUT':    
        result = requests.put(url, data=request.data, headers=headers)    
    else:    
        result = requests.get(url, headers=headers)    
    
    response = HttpResponse(
        content=result.content,
        status=result.status_code,
        content_type=result.headers['Content-Type']
    )

    return response    
    
@csrf_exempt
@api_view(['GET',])
def barangay_transaction(request):

    if request.is_secure():
        url = 'https://' + request.META['HTTP_HOST']
    else:
        url = 'http://' + request.META['HTTP_HOST']
        
    city_id = request.GET.get('city_id', '')        
    
    url = url + F'/api/barangays/?city_id={city_id}'      
    headers = {'Authorization': 'Token ' + settings.TOKEN}
    
    if request.method == 'POST':
        result = requests.post(url, data=request.data, headers=headers)    
    elif request.method == 'PUT':    
        result = requests.put(url, data=request.data, headers=headers)    
    else:    
        result = requests.get(url, data=request.data, headers=headers)    
    
    response = HttpResponse(
        content=result.content,
        status=result.status_code,
        content_type=result.headers['Content-Type']
    )

    return response    

@api_view(['GET',])
def city_transaction(request):

    if request.is_secure():
        url = 'https://' + request.META['HTTP_HOST']
    else:
        url = 'http://' + request.META['HTTP_HOST']
        
    province_id = request.GET.get('province_id', '')        
    url = url + F'/api/cities/?province_id={province_id}'    
    headers = {'Authorization': 'Token ' + settings.TOKEN}
    
    if request.method == 'POST':
        result = requests.post(url, data=request.data, headers=headers)    
    elif request.method == 'PUT':    
        result = requests.put(url, data=request.data, headers=headers)    
    else:    
        result = requests.get(url, data=request.data, headers=headers)    
    
    response = HttpResponse(
        content=result.content,
        status=result.status_code,
        content_type=result.headers['Content-Type']
    )

    return response 

@api_view(['GET',])
def province_transaction(request):

    if request.is_secure():
        url = 'https://' + request.META['HTTP_HOST']
    else:
        url = 'http://' + request.META['HTTP_HOST']
        
    country_id = request.GET.get('country_id', '')        
    url = url + F'/api/provinces/?country_id={country_id}'    
    headers = {'Authorization': 'Token ' + settings.TOKEN}
    
    if request.method == 'POST':
        result = requests.post(url, data=request.data, headers=headers)    
    elif request.method == 'PUT':    
        result = requests.put(url, data=request.data, headers=headers)    
    else:    
        result = requests.get(url, data=request.data, headers=headers)    
    
    response = HttpResponse(
        content=result.content,
        status=result.status_code,
        content_type=result.headers['Content-Type']
    )

    return response

@api_view(['GET','PUT', 'POST', ])
def profile_transaction(request):
    
    if request.is_secure():
        url = 'https://' + request.META['HTTP_HOST']
    else:
        url = 'http://' + request.META['HTTP_HOST']
        
    profile_id = request.GET.get('profile_id', '')        
    url = url + F'/api/profiles/?profile_id={profile_id}'    
    headers = {'Authorization': 'Token ' + settings.TOKEN}
    
    if request.method == 'POST':
        result = requests.post(url, data=request.data, headers=headers)    
    elif request.method == 'PUT':    
        result = requests.put(url, data=request.data, headers=headers)    
    else:    
        result = requests.get(url, data=request.data, headers=headers)    
    
    response = HttpResponse(
        content=result.content,
        status=result.status_code,
        content_type=result.headers['Content-Type']
    )

    return response 


