from django.views.generic import (View, TemplateView)
from django.shortcuts import redirect
from django.conf import settings
from tracker.common import Repeated 
from tracker.dbs import DB
from django.db import connection 

from django.http import HttpResponse, JsonResponse

import json

class Login(TemplateView):
    template_name='login/login.html'

class Verify(View):
    def post(self, request):
        if request.method == 'POST' and request.POST:
            # db_conn = connections['default']
            # try:
            #     c = db_conn.cursor()
            # except OperationalError:
            #     connected = False
            # else:
            #     connected = True            
            # return HttpResponse(connected)     
            try:
                db = DB()                
                context = {}
                list_data={}
                
                data = json.dumps(request.POST)
                context = json.loads(data)
                context['UserLogin'] = Repeated.empty_to_null(request.POST.get('username'))   
                context['UserPassword'] = settings.DB_PREFIX + Repeated.empty_to_null(request.POST.get('password'))  
                context['UserLogLocalIP'] = Repeated.empty_to_null(Repeated.get_local_hostname_ip('ip'))   
                context['UserLogIP'] = Repeated.empty_to_null(Repeated.external_ip())   
                context['UserLogHost'] = Repeated.empty_to_null(Repeated.get_local_hostname_ip('host'))   
                context['SystemCode'] = 'GTC'
                context['Flag'] = 13   
                context['SPName'] = 'verifylogin' 

                list_data = db.profiles(context)

                if len(list_data) and list_data[0]['IsSuccess'] == '1':
                    request.session['Attachment']           = list_data[0]['Attachment']
                    request.session['AttachmentOrig']       = list_data[0]['AttachmentOrig']
                    request.session['BranchCode']           = list_data[0]['BranchCode']
                    request.session['BranchID']             = list_data[0]['BranchID']
                    request.session['BranchName']           = list_data[0]['BranchName']
                    request.session['DateAdded']            = str( list_data[0]['DateAdded'] )
                    request.session['EntityName']           = list_data[0]['EntityName']
                    request.session['EntityNameID']         = list_data[0]['EntityNameID']
                    request.session['IsSuccess']            = list_data[0]['IsSuccess']
                    request.session['ModuleController']     = list_data[0]['ModuleController']
                    request.session['ProfileBirthdate']     = str(list_data[0]['ProfileBirthdate'])
                    request.session['ProfileExtName']       = list_data[0]['ProfileExtName']
                    request.session['ProfileFirstName']     = list_data[0]['ProfileFirstName']
                    request.session['ProfileID']            = list_data[0]['ProfileID']
                    request.session['ProfileLastName']      = list_data[0]['ProfileLastName']
                    request.session['ProfileMiddleName']    = list_data[0]['ProfileMiddleName']
                    request.session['Result']               = list_data[0]['Result']
                    request.session['SystemID']             = list_data[0]['SystemID']
                    request.session['SystemName']           = list_data[0]['SystemName']
                    request.session['UserLoginID']          = list_data[0]['UserLoginID']
                    request.session['UserLoginName']        = list_data[0]['UserLoginName']                    
                    request.session['IsLogin']              = True    
                                    
                    return JsonResponse({'result':list_data}, safe = False) 
                else:
                    request.session.flush()
                    
                    return JsonResponse({'result':[Repeated.no_record()]}) 

            except Exception as ex:
                return HttpResponse(ex)
        else:
            return JsonResponse({'result':[Repeated.form_method_invalid()]})

def logout(request):
    try:
        request.session.flush()   
    except KeyError:
        pass
    return redirect('/')  