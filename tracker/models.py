from django.db import models

# Create your models here.

class Cities(models.Model):
    city_id = models.AutoField(primary_key = True)
    city_name = models.CharField(max_length=100)
    
    def __str__(self):
        return self.city_name
    
class Barangays(models.Model):
    barangay_id = models.AutoField(primary_key = True)
    barangay_name = models.CharField(max_length=100)
    city_fk = models.ForeignKey(Cities, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.barangay_name    
    
class Schools(models.Model):
    school_id = models.AutoField(primary_key=True)
    school_code = models.CharField(max_length=20, blank=True, null=True, unique=True)
    school_name = models.CharField(max_length=100)
    school_email = models.CharField(max_length=200)
    school_contact = models.CharField(max_length=50)
    school_street = models.CharField(max_length=200, blank=True, default='')
    barangay_fk_id = models.IntegerField(blank=True, null=True)
    
class Profiles(models.Model):
    profile_id = models.AutoField(primary_key=True)
    profile_firstname = models.CharField(max_length=100)
    profile_lastname = models.CharField(max_length=100)
    profile_birthday = models.DateField()
    
class SchoolAdmin(models.Model):
    contact_person_id = models.AutoField(primary_key=True)
    contact_person_name = models.CharField(max_length=200, blank=True, null=True)
    school_fk = models.ForeignKey(Schools, on_delete=models.CASCADE)    
    profile_fk_id = models.IntegerField(blank=True, null=True)
    
    
class Students(models.Model):
    student_id = models.AutoField(primary_key=True)
    student_name = models.CharField(max_length=100, blank=True, null=True)
    company_name = models.CharField(max_length=100, blank=True, null=True)
    employee_status = models.IntegerField()
    profile_fk_id = models.IntegerField(blank=True, null=True)

    
class Courses(models.Model):
    course_id = models.AutoField(primary_key=True)
    course_code = models.CharField(max_length=100, blank=True, null=True, unique=True)
    course_name = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.course_name    
    
class StudentDetails(models.Model):
    student_detail_id = models.AutoField(primary_key=True)
    school_year_graduated = models.DateField()
    course_fk = models.ForeignKey(Courses, on_delete=models.CASCADE)            
    school_fk = models.ForeignKey(Schools, on_delete=models.CASCADE)            
    student_fk = models.ForeignKey(Students, on_delete=models.CASCADE)    
