from .models import Courses
from .serializers import CourseSerializer
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from django.db import transaction
from tracker.common import Repeated


class Coursesv2(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        course_name = request.GET.get('course_name', '')
        if course_name:
            queryset = Courses.objects.filter(
                course_name__icontains=course_name)
        else:
            queryset = Courses.objects.all()

        serializer = CourseSerializer(queryset, many=True)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        print('put')

    def post(self, request, format=None):

        with transaction.atomic():
            max_id = Courses.objects.last().course_id + 1
            course_data = request.POST.copy()
            course_data['course_code'] = Repeated.my_random_string(
                6) + '-' + str(max_id)
            course = CourseSerializer(data=course_data)
            if course.is_valid(raise_exception=True):
                course.save()
                course_data['course_id'] = course.data['course_id']
                return Response(course_data,
                                status=status.HTTP_201_CREATED)
            else:
                transaction.set_rollback(True)
                return Response(course.errors,
                                status=status.HTTP_400_BAD_REQUEST)


class CourseDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Courses.objects.all()
    serializer_class = CourseSerializer
