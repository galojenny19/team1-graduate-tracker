$(document).ready(function() {
    var Registration = {
        Init: function(config){
            this.config = config;
            this.BindEvents();
        },
        BindEvents: function(){
            var $this = this.config;
                $this.tab_admin.on('click', {param : 1}, this.ToogleHeader);
                $this.tab_student.on('click', {param : 2}, this.ToogleHeader);
                $this.btn_admin_register.on('click', {param : 1}, this.RegisterSchoolAdmin);
                $this.btn_student_register.on('click', {param : 1}, this.RegisterStudents);
                $this.sel_admin_city.on('change', {param : 1}, this.BarangayList);
                $this.sel_admin_province.on('change', {param : 1}, this.CityList);
                $this.in_admin_confirm_password.on('keyup', {param : 1}, this.ConfirmPassword);
                $this.in_student_confirm_password.on('keyup', {param : 2}, this.ConfirmPassword);

                Registration.OnloadPage(1);
        },
        OnloadPage: function(e){
            var $self = Registration.config,
                $route = (typeof(e) == 'object') ? e.data.param : e;

                switch($route){
                    case 1:
                        $self.form_admin.prop('hidden', false);
                        Registration.ProvinceList(1, null);
                        Registration.SchoolList(1, null);
                        Registration.CourseList(1, null);
                    break;
                }
        },
        ToogleHeader: function(e, data){
             var $self = Registration.config,
                 $route = typeof(e) == 'object' ? e.data.param: e;
                 
                 switch($route){
                    case 1: // tab-admin is active
                        $(this).addClass('active');
                        $self.tab_student.removeClass('active');
                        $self.form_student.prop('hidden', true);
                        $self.form_admin.prop('hidden', false);
 
                    break;
                    case 2: // tab-student is active
                        $(this).addClass('active');
                        $self.tab_admin.removeClass('active');
                        $self.form_student.prop('hidden', false);
                        $self.form_admin.prop('hidden', true);
                    break;
                 }
        },
        ClearForm: function(e){
            var $self = Registration.config,
                $route = typeof(e) == 'object' ? e.data.param : e;

                switch($route){
                    case 1: //clear admin form
                        $self.sel_admin_city.val(0)           
                        $self.sel_admin_barangay.val(0);        
                        $self.sel_admin_province.val(0);        
                        $self.in_admin_first_name.val('');     
                        $self.in_admin_middle_name.val('');        
                        $self.in_admin_last_name.val('');        
                        $self.in_admin_suffix.val('');        
                        $self.in_admin_birthday.val('');         
                        $self.in_admin_school.val('');          
                        $self.in_admin_school_code.val('');        
                        $self.in_admin_street.val('');
                        $self.in_admin_contact_number.val('');
                        $self.in_admin_email.val('');            
                        $self.in_admin_password.val('');          
                        $self.in_admin_confirm_password.val('');  
                    break;
                    case 2: // clear student form
                        $self.in_student_first_name.val('');
                        $self.in_student_last_name.val('');
                        $self.in_student_middle_name.val('');
                        $self.in_student_suffix.val('');
                        $self.in_student_birthday.val('');
                        $self.sel_student_school.val('');
                        $self.sel_student_course.val('');
                        $self.in_student_year_graduated.val('');
                        $self.in_student_company.val('');
                        $self.sel_student_status.val(0);
                        $self.in_student_email.val('');
                        $self.in_student_password.val('');
                        $self.in_student_confirm_password.val('');
                        $self.in_student_contact.val('');
                    break;
                }
                
        },
        ConfirmPassword: function(e){
            var $self = Registration.config,
                $route = (typeof(e) == 'object') ? e.data.param : e;

                switch($route){
                    case 1:
                        if($self.in_admin_password.val() != $self.in_admin_confirm_password.val()){
                            $self.password_notif.prop('hidden', false);
                        }
                        if($self.in_admin_password.val() == $self.in_admin_confirm_password.val()){
                            $self.password_notif.prop('hidden', true);                            
                        }
                        if($self.in_admin_password.val() == ''){
                            $self.password_notif.prop('hidden', true); 
                        }
                        if($self.in_admin_confirm_password.val() == ''){
                            $self.password_notif.prop('hidden', true);  
                        }
                    break;
                    case 2:
                        if($self.in_student_password.val() != $self.in_student_confirm_password.val()){
                            $self.password_student_notif.prop('hidden', false);
                        }
                        if($self.in_student_password.val() == $self.in_student_confirm_password.val()){
                            $self.password_student_notif.prop('hidden', true);                            
                        }
                        if($self.in_student_password.val() == ''){
                            $self.password_student_notif.prop('hidden', true); 
                        }
                        if($self.in_student_confirm_password.val() == ''){
                            $self.password_student_notif.prop('hidden', true);  
                        }
                    break;
                }
        },
        RegisterSchoolAdmin: function(e, data){
            var $self = Registration.config,
                $base_host  = $.trim($self.content_wrapper.attr('data-host')),
                $route = (typeof(e) == 'object') ? e.data.param : e;
                
                switch($route){
                    case 1:
                        if($self.in_admin_first_name.val() == ''){
                            Registration.IziToast(4,'First Name is required!');
                            e.preventDefault();
                            return $self.in_admin_first_name.focus();
                        }else if($self.in_admin_last_name.val() == ''){
                            Registration.IziToast(4,'Last Name is required!');
                            e.preventDefault();
                            return $self.in_admin_last_name.focus();
                        }else if($self.in_admin_birthday.val() == ''){
                            Registration.IziToast(4,'Birthday is required!');
                            e.preventDefault();
                            return $self.in_admin_birthday.focus();
                        }else if($self.in_admin_confirm_password.val() == ''){
                            e.preventDefault();
                            $self.btn_admin_register.prop('disabled', true);
                            return $self.in_admin_birthday.focus();
                        }
                        iziToast.show({
                            theme: 'dark',
                            icon: 'icon-person',
                            title: 'System Message!',
                            message: 'Are you sure you want to save this School Administrator profile?',
                            position: 'center',
                            progressBarColor: 'rgb(0, 255, 184)',
                            zindex: 1051,
                            timeout: 20000,
                            close: false,
                            overlay: true,
                            drag: false,
                            displayMode: 'once',
                            buttons: [
                                ['<button>Ok</button>', function (instance, toast) {
                                    $data = $self.form_admin.serializeArray();
                                    $data.push({name: 'barangay_fk_id', value: $self.sel_admin_barangay.find('option:selected').val()})
                                    e.preventDefault();
                                    Registration.CallAjax('/api/profile_transaction/', $data, 1, 'POST');
                                    iziToast.destroy();
                                }, true], // true to focus
                                ['<button>Close</button>', function (instance, toast) {
                                    instance.hide({
                                        transitionOut: 'fadeOutUp',
                                        onClosing: function(instance, toast, closedBy){ 
                                            Registration.ClearForm(1);
                                            console.info('closedBy: ' + closedBy); 
                                        }
                                    }, toast, 'buttonName');
                                }]
                            ],
                            onOpening: function(instance, toast){ console.info('callback abriu!'); },
                            onClosing: function(instance, toast, closedBy){ console.info('closedBy: ' + closedBy); }
                        });  
                    break;
                    case 2: 
                        Registration.IziToast(1,'School Admin Profile is registered successfully!');
                        Registration.ClearForm(1);
                        window.location.replace(`${$base_host}`);
                        console.log(data);
                    break;
                }
        },
        RegisterStudents: function(e, data){
            var $self = Registration.config,
            $base_host  = $.trim($self.content_wrapper.attr('data-host')),
            $route = (typeof(e) == 'object') ? e.data.param : e;
            
            switch($route){
                case 1:
                    if($self.in_student_first_name.val() == ''){
                        Registration.IziToast(4,'First Name is required!');
                        e.preventDefault();
                        return $self.in_student_first_name.focus();
                    }else if($self.in_student_last_name.val() == ''){
                        Registration.IziToast(4,'Last Name is required!');
                        e.preventDefault();
                        return $self.in_student_last_name.focus();
                    }else if($self.in_student_birthday.val() == ''){
                        Registration.IziToast(4,'Birthday is required!');
                        e.preventDefault();
                        return $self.in_student_birthday.focus();
                    }
                    iziToast.show({
                        theme: 'dark',
                        icon: 'icon-person',
                        title: 'System Message!',
                        message: 'Are you sure you want to save this Alumna profile?',
                        position: 'center',
                        progressBarColor: 'rgb(0, 255, 184)',
                        zindex: 1051,
                        timeout: 20000,
                        close: false,
                        overlay: true,
                        drag: false,
                        displayMode: 'once',
                        buttons: [
                            ['<button>Ok</button>', function (instance, toast) {
                                $data = $self.form_student.serializeArray();
                                // console.log($data);return;
                                $data.push(
                                    {name: 'employee_status', value: $self.sel_student_status.find('option:selected').val()},
                                    {name: 'course_fk', value: $self.sel_student_course.find('option:selected').val()},
                                    {name: 'school_fk', value: $self.sel_student_school.find('option:selected').val()},
                                    {name: 'UserLoginName', value: $self.in_student_email.val()}
                                );
                                e.preventDefault();
                                Registration.CallAjax('/api/student_transaction/', $data, 3, 'POST');
                                iziToast.destroy();
                            }, true], // true to focus
                            ['<button>Close</button>', function (instance, toast) {
                                instance.hide({
                                    transitionOut: 'fadeOutUp',
                                    onClosing: function(instance, toast, closedBy){ 
                                        Registration.ClearForm(1);
                                        console.info('closedBy: ' + closedBy); 
                                    }
                                }, toast, 'buttonName');
                            }]
                        ],
                        onOpening: function(instance, toast){ console.info('callback abriu!'); },
                        onClosing: function(instance, toast, closedBy){ console.info('closedBy: ' + closedBy); }
                    });  
                    e.preventDefault();
                break;
                case 2: 
                    Registration.IziToast(1,'School Alumna Profile is registered successfully!');                
                    Registration.ClearForm(2);
                    window.location.replace(`${$base_host}`);
                    console.log(data);
                break;
            }
        },
        SchoolList: function(e, data){
            var $self = Registration.config,
                $route = typeof(e) == 'object' ? e.data.param : e;

                switch($route){
                    case 1: 
                        $data = $self.dummy_form.serializeArray();
                        Registration.CallAjax('/api/school_transaction/', $data, 6, 'POST' );
                    break;
                    case 2:
                        var len = data.length, txt="";
                        if(len > 0){
                            for(i=0;i<len;i++){
                                txt += ` <option value="${data[i]['school_id']}">${data[i]['school_name']}</option>`;
                            }
                        }else{
                            txt +=`<option selected> Select School...</option>`;
                        }
                        if(txt !=''){
                            $self.sel_student_school.append(txt);    
                        }
                    break;
                }
        },
        CourseList: function(e, data){
            var $self = Registration.config,
                $route = typeof(e) == 'object' ? e.data.param : e;

                switch($route){
                    case 1: 
                        $data = $self.dummy_form.serializeArray();
                        Registration.CallAjax('/api/course_transaction/', $data, 7, 'GET' );
                    break;
                    case 2:
                        var len = data.length, txt="";
                        if(len > 0){
                            for(i=0;i<len;i++){
                                txt += ` <option value="${data[i]['course_id']}">${data[i]['course_name']}</option>`;
                            }
                        }else{
                            txt +=`<option selected> Select Course...</option>`;
                        }
                        if(txt !=''){
                            $self.sel_student_course.append(txt);    
                        }
                    break;
                }
        },
        ProvinceList: function(e, data){
            var $self = Registration.config, 
                $route = typeof(e) == 'object' ? e.data.param : e;

                switch($route){
                    case 1:
                        $data = $self.dummy_form.serializeArray();   
                        Registration.CallAjax('/api/province_transaction/',{$data}, 8, 'GET');
                    break;
                    case 2:
                        var len = data.length, txt="";
                        if(len > 0){
                            for(i=0;i<len;i++){
                                txt += ` <option value="${data[i]['ProvinceID']}">${data[i]['ProvinceName']}</option>`;
                            }
                        }else{
                            txt +=`<option selected> Select Province...</option>`;
                        }
                        if(txt !=''){
                            $self.sel_admin_province.empty();    
                            $self.sel_admin_province.append(txt);    
                            if($self.sel_admin_province.find('option:selected').val() != "0"){
                                Registration.CityList(1, null);  
                            }
                        }
                    break;
                }
        },
        CityList: function(e, data){
            var $self = Registration.config, 
                $route = typeof(e) == 'object' ? e.data.param : e;

                switch($route){
                    case 1:
                        // $data = $self.dummy_form.serializeArray();   
                        $data = {'province_id' : $self.sel_admin_province.find('option:selected').val()}
                        Registration.CallAjax('/api/city_transaction/',$data, 4, 'GET');
                    break;
                    case 2:
                        var len = data.length, txt="";
                        if(len > 0){
                            for(i=0;i<len;i++){
                                txt += ` <option value="${data[i]['CityMunicipalityID']}">${data[i]['CityMunicipalityName']}</option>`;
                            }
                        }else{
                            txt +=`<option selected> Select City...</option>`;
                        }
                        if(txt !=''){
                            $self.sel_admin_city.empty();    
                            $self.sel_admin_city.append(txt);    
                            if($self.sel_admin_city.find('option:selected').val() != "0"){
                                Registration.BarangayList(1, null);   
                            }
                        }
                    break;
                }
        },
        BarangayList: function(e, data){
            var $self = Registration.config,
                $route = typeof(e) == 'object' ? e.data.param : e;

                switch($route){
                    case 1:
 
                        $data = {"city_id": $self.sel_admin_city.find('option:selected').val()}
                        Registration.CallAjax('/api/barangay_transaction/',$data, 5, 'GET');
                    break;
                    case 2:
                        var len = data.length, txt="";
                        if(len > 0){
                            for(i=0;i<len;i++){
                                txt += ` <option value="${data[i]['BarangayID']}">${data[i]['BarangayName']}</option>`;
                            }
                        }else{
                            txt +=`<option selected> Select Barangay...</option>`;
                        }
                        if(txt !=''){
                            $self.sel_admin_barangay.empty();
                            $self.sel_admin_barangay.append(txt);                            
                        }
                    break;
                }
        },
        IziToast: function(e , message) {
            var $self = Registration.config;
                $tmp  = (typeof(e) == 'object' ) ? e.data.param : e ;

                switch($tmp) {
                    case 1: //Success
                        iziToast.success({
                            title    : 'Success',
                            message  : message,
                            position : 'topRight',
                            icon     : 'fa fa-thumbs-up'
                        });
                    break;
                    case 2: //Error
                        iziToast.error({
                            title    : 'Error',
                            message  : message,
                            position : 'topRight',
                            icon     : 'fa fa-exclamation'
                        });
                    break;
                    case 3: //Info
                        iziToast.info({
                            title    : 'Info',
                            message  : message,
                            position : 'topRight',
                            icon     : 'fa fa-info'
                        });
                    break;
                    case 4: //Warning
                        iziToast.warning({
                            title    : 'Warning',
                            message  :  message,
                            position : 'topRight',
                            icon     : 'fa fa-exclamation-triangle'
                        });
                    break;

                }
        }, 
        CallAjax: function(url, data, route, method_type){
            var $self       = Registration.config, timer, data_object = {},
                $base_host  = $.trim($self.content_wrapper.attr('data-host')),
                $url        =  $base_host + url;

                // console.log($url)
            $.ajax({
                type: method_type,
                url: $url,
                data: data,
                dataType:'json',
                beforeSend: function(){
                    timer && clearTimeout(timer);
                    timer = setTimeout(function()
                    {
                        $("body").addClass("loading"); 
                    },
                    1000);                    

                    switch(route){
                        case 2: 
                                $self.spinner_border.removeClass('hidden');
                        break;
                    }                    
                },
                complete: function(){
                    clearTimeout(timer);
                    $("body").removeClass("loading"); 

                    switch(route){
                        case 2: 
                                $self.spinner_border.addClass('hidden');
                        break;
                    }                    
                },                
                success: function(evt){ 
                    console.log(typeof(evt));
                    if(evt){
                        switch(route){
                            case 1: Registration.RegisterSchoolAdmin(2, evt); break; 
                            case 2: Registration.RegisterSchools(2, evt); break; 
                            case 3: Registration.RegisterStudents(2, evt); break; 
                            case 4: Registration.CityList(2, evt); break;  
                            case 5: Registration.BarangayList(2, evt); break; 
                            case 6: Registration.SchoolList(2, evt); break;  
                            case 7: Registration.CourseList(2, evt); break;  
                            case 8: Registration.ProvinceList(2, evt); break;  
                        }    
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log('error: ' + textStatus + ': ' + errorThrown);
                }
            }); 
        }//end sa callajax  
    }
    Registration.Init({
        btn_admin_register              : $('#btn-admin-register'),
        btn_student_register            : $('#btn-student-register'),
        content_wrapper                 : $('.content-wrapper'),
        dummy_form                      : $('dummy-form'),
        form_student                    : $('#form-student'),
        form_admin                      : $('#form-admin'),
        tab_admin                       : $('#tab-admin'),
        tab_student                     : $('#tab-student'),
        password_notif                  : $('#password-notif'),

        sel_admin_city                  : $('#sel-admin-city'),
        sel_admin_barangay              : $('#sel-admin-barangay'),
        sel_admin_province              : $('#sel-admin-province'),
        in_admin_first_name             : $('#in-admin-first-name'),
        in_admin_last_name              : $('#in-admin-last-name'),
        in_admin_middle_name            : $('#in-admin-middle-name'),
        in_admin_suffix                 : $('#in-admin-suffix'),
        in_admin_birthday               : $('#in-admin-birthday'),
        in_admin_school                 : $('in-admin-school'),
        in_admin_school_code            : $('#in-admin-school-code'),
        in_admin_street                 : $('#in-admin-street'),
        in_admin_contact_number         : $('#in-admin-contact-number'),
        in_admin_email                  : $('#in-admin-email'),
        in_admin_password               : $('#in-admin-password'),
        in_admin_confirm_password       : $('#in-admin-confirm-password'),
        
        in_student_first_name           : $('#in-student-first-name'),
        in_student_last_name            : $('#in-student-last-name'),
        in_student_middle_name          : $('#in-student-middle-name'),
        in_student_suffix               : $('#in-student-suffix'),
        in_student_birthday             : $('#in-student-birthday'),
        sel_student_school              : $('#sel-student-school'),
        sel_student_course              : $('#sel-student-course'),
        in_student_year_graduated       : $('#in-student-year-graduated'),
        in_student_company              : $('#in-student-company'),
        sel_student_status              : $('#sel-student-status'),
        in_student_email                : $('#in-student-email'),
        in_student_password             : $('#in-student-password'),
        in_student_confirm_password     : $('#in-student-confirm-password'),
        in_student_contact              : $('#in-student-contact'),

        password_student_notif          : $('#password-student-notif')
    });
});