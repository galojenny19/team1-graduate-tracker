$(document).ready(function(){
    var Login = {
        Init: function(config){
            this.config = config;
            Login.BindEvents();
        },
        BindEvents: function(){
            $this = this.config,
            
            $this.btn_login.on('click', {param : 1}, this.LoginVerify)
            $this.in_password.on('keypress',{param : 1}, this.PressEnter);
            
            Login.OnloadPage(1);

        },
        OnloadPage: function(e){
            var $self = Login, config,
                $base_host  = $.trim($this.content_wrapper.attr('data-host')),
                $route = (typeof(e) == 'object') ? e.data.param : e;

                switch($route){
                    case 1:
                        $img = $base_host + '/static/img/vasily-koloda-unsplash.jpg';
                        $('body').css({'background-image': `url(${$img}) `});
                    break;
                }
        },
        PressEnter: function(e){
            var $self = Login.config,
                $route = (typeof(e) == 'object') ? e.data.param : e;

                switch($route){
                    case 1:
                        if(e.keycode == 13){
                            $self.btn_login.click();
                            e.preventDefault();
                        }
                    break;
                }
        },
        ClearFields: function(){
            var $self = Login.config,
                $route = (typeof(e) == 'object') ? e.data.param : e ;

                switch($route){
                    case 1:
                        $self.in_password.val('');
                        $self.in_username.val('');
                    break;
                }

        },
        LoginVerify: function(e, data){
            var $self       = Login.config,
                $base_host  = $.trim($this.content_wrapper.attr('data-host')),
                $route      = (typeof(e) == 'object') ? e.data.param : e;

                switch($route){
                    case 1:
                        if($self.in_password.val() == ''){
                            Login.IziToast(3,'Password is missing!');
                            return $self.in_password.focus()
                        }
                        $data = $self.login_form.serializeArray();

                        Login.CallAjax('/login_verify/', $data, 1, 'POST' );
                    break;
                    case 2:
                        data = data['result']
                        if(data[0]['IsSuccess'] != 0){
                            Login.IziToast(1,'Logged In successfully!');
                            window.location.replace(`${$base_host+'/'+data[0]['ModuleController']+'/'}`);
                        }else{
                            Login.IziToast(2, $data[0]['Result']);
                        }
                    break;
                }
            
        },
        IziToast: function(e , message) {
            var $self = Login.config;
                $tmp  = (typeof(e) == 'object' ) ? e.data.param : e ;

                switch($tmp) {
                    case 1: //Success
                        iziToast.success({
                            title    : 'Success',
                            message  : message,
                            position : 'topRight',
                            icon     : 'fa fa-thumbs-up'
                        });
                    break;
                    case 2: //Error
                        iziToast.error({
                            title    : 'Error',
                            message  : message,
                            position : 'topRight',
                            icon     : 'fa fa-exclamation'
                        });
                    break;
                    case 3: //Info
                        iziToast.info({
                            title    : 'Info',
                            message  : message,
                            position : 'topRight',
                            icon     : 'fa fa-info'
                        });
                    break;
                    case 4: //Warning
                        iziToast.warning({
                            title    : 'Warning',
                            message  :  message,
                            position : 'topRight',
                            icon     : 'fa fa-exclamation-triangle'
                        });
                    break;

                }
        }, 
        CallAjax: function(url, data, route, method_type){
            var $self       = Login.config, timer, data_object = {},
                $base_host  = $.trim($self.content_wrapper.attr('data-host')),
                $url        =  $base_host + url;

                // console.log($url)
            $.ajax({
                type: method_type,
                url: $url,
                data: data,
                dataType:'json',
                beforeSend: function(){
                    timer && clearTimeout(timer);
                    timer = setTimeout(function()
                    {
                        $("body").addClass("loading"); 
                    },
                    1000);                    

                    switch(route){
                        case 2: 
                                $self.spinner_border.removeClass('hidden');
                        break;
                    }                    
                },
                complete: function(){
                    clearTimeout(timer);
                    $("body").removeClass("loading"); 

                    switch(route){
                        case 2: 
                                $self.spinner_border.addClass('hidden');
                        break;
                    }                    
                },                
                success: function(evt){ 
                    console.log(typeof(evt));
                    if(evt){
                        switch(route){
                            case 1: Login.LoginVerify(2, evt); break; 
                        }    
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log('error: ' + textStatus + ': ' + errorThrown);
                }
            }); 
        }//end sa callajax  
    }
    Login.Init({
        login_form              : $('#login-form'),
        in_password             : $('#in-password'),
        in_username             : $('#in-username'),
        btn_login               : $('#btn-login'),
        content_wrapper         : $('.content-wrapper'),
    })
});