$(document).ready(function(){
    var Student = {
        Init: function(config){
            this.config = config;
            Student.BindEvents();
        },
        BindEvents: function(){
            var $this = this.config;
                $this.btn_view_more_student.on('click', {param:1}, this.ViewMoreStudents);
                $this.btn_view_more_school.on('click', {param:1}, this.ViewMoreSchools);
                $this.btn_search.on('click', {param:1}, this.SearchForStudents);
                $this.in_search.on('keypress', {param:3}, this.SearchForStudents);
                $this.a_view_profile.on('click', {param:1}, this.ViewProfile);
                $this.logout.on('click', {param:1}, this.Logout);

        },
        ViewMoreStudents: function(e, data){
            var $self = Student.config,
                $route = (typeof(e) == 'object') ? e.data.param : e;

                switch($route){
                    case 1:
                        Student.CallAjax('/api/student_transaction/', null, 1, 'GET');
                    break;
                    case 2:
                        $self.student_table.prop('hidden', false);
                        $self.school_table.prop('hidden', true);

                        $self.title_school.prop('hidden', true);
                        $self.title_student.prop('hidden', false);

                        $self.nav_search.prop('hidden', false);
                        var len = data.length, txt="";
                        $self.tbl_students.empty();    
                        if(len > 0){
                            for(i=0;i<len;i++){
                                txt += `<tr>
                                            <td>${data[i]['student_name']}</td>
                                            <td>${data[i]['company_name']}</td>
                                            <td>${data[i]['employee_status']}</td>
                                        </tr>`;
                            }
                        }else{
                            txt +=`<tr> <td> colspan="3">No Record Found</td></tr>`;
                        }
                        if(txt !=''){
                            $self.tbl_students.append(txt);    
                        }
                    break;
                }
        },
        ViewMoreSchools: function(e, data){
            var $self = Student.config,
                $route = (typeof(e) == 'object') ? e.data.param : e;

                switch($route){
                    case 1:
                        Student.CallAjax('/api/school_transaction/', null, 2, 'GET');
                    break;
                    case 2:
                        // console.log(data); return

                        $self.school_table.prop('hidden', false);
                        $self.student_table.prop('hidden', true);

                        $self.title_school.prop('hidden', false);
                        $self.title_student.prop('hidden', true);

                        $self.nav_search.prop('hidden', true);

                        var len = data.length, txt="";
                        $self.school_card_body.empty();    
                        if(len > 0){
                            for(i=0;i<len;i++){
                                txt += `<tr>
                                            <td>${data[i]['school_name']}</td>
                                            <td>${data[i]['school_code']}</td>
                                            <td>${data[i]['barangay_fk_id']}</td>
                                            <td>${data[i]['school_street']}</td>
                                            <td>${data[i]['school_email']}</td>
                                            <td>${data[i]['school_contact']}</td>
                                        </tr>
                                        `;
                            }
                        }else{
                            txt +=`<tr> <td> colspan="3">No Record Found</td></tr>`;
                        }
                        if(txt !=''){
                            $self.school_card_body.append(txt);    
                        }
                    break;
                }
        },
        SearchForStudents: function(e, data){
            var $self = Student.config,
                $route = (typeof(e) == 'object') ? e.data.param : e;

                switch($route){
                    case 1:

                        $data = $self.form_search.serializeArray();
                        Student.CallAjax('/api/student_list/',  $data, 3, 'GET');
                        e.preventDefault();
                    break;
                    case 2:
                        var len = data.length, txt="";
                        $self.tbl_students.empty();    
                        if(len > 0){
                            for(i=0;i<len;i++){
                                txt += `<tr>
                                            <td>${data[i]['student_fk__student_name']}</td>
                                            <td>${data[i]['student_fk__company_name']}</td>
                                            <td>${data[i]['student_fk__employee_status']}</td>
                                        </tr>`;
                            }
                        }else{
                            txt +=`<tr> <td> colspan="3">No Record Found</td></tr>`;
                        }
                        if(txt !=''){
                            $self.tbl_students.append(txt);    
                        }
                    break;
                    case 3:
                        if(e.keycode == 13){
                            $self.btn_search.click();
                            e.preventDefault();
                        }
                    break;
                }
        },
        ViewProfile: function(e, data){
            var $self = Student.config,
                $route = (typeof(e) == 'object') ? e.data.param : e;

                switch($route){
                    case 1:
                        $self.in_first_name.val($(this).attr('data-profile-firstname'));
                        $self.in_middle_name.val($(this).attr('data-profile-middlename'));
                        $self.in_last_name.val($(this).attr('data-profile-lastname'));
                        $self.in_birthday.val($(this).attr('data-profile-birthday'));
                        $self.in_user_name_login.val($(this).attr('data-profile-userlogin-name'));
                        $self.modal_profile.modal('show');
                    break;
                }
        },
        EditProfile: function(e, data){
            var $self = Student.config,
                $route = (typeof(e) == 'object') ? e.data.param : e;

                switch($route){
                    case 1:
                        
                    break;
                }
        },
        CloseModal: function(e){
            var $self = Student.config,
                $route = (typeof(e) == 'object') ?  e.data.param : e;

                switch($route){
                    case 1: // view profile
                        $self.btn_close_modal.modal('hide');
                        $self.in_first_name.val();
                        $self.in_middle_name.val();
                        $self.in_last_name.val();
                        $self.in_birthday.val();
                        $self.in_user_name_login.val();
                    break;
                    case 2: // update profile
                        $self.btn_close_modal.modal('hide');
                    break;
                }
        },
        Logout: function(e){
            var $route      = (typeof e == 'object') ? e.data.param : e, 
                $self       = Student.config,
                $url       = $(this).attr('data-href'),
                $base_host  = $.trim($self.content_wrapper.attr('data-host'));

                switch($route){
                    case 1:
                            iziToast.show({
                                theme: 'dark',
                                icon: 'icon-person',
                                title: 'System Message!',
                                message: 'Are you sure you want to logout?',
                                position: 'center',
                                progressBarColor: 'rgb(0, 255, 184)',
                                zindex: 1051,
                                timeout: 20000,
                                close: false,
                                overlay: true,
                                drag: false,
                                displayMode: 'once',
                                buttons: [
                                    ['<button>Ok</button>', function (instance, toast) {
                                        window.location.replace($base_host + $url);
                                        iziToast.destroy();
                                    }, true], // true to focus
                                    ['<button>Close</button>', function (instance, toast) {
                                        instance.hide({
                                            transitionOut: 'fadeOutUp',
                                            onClosing: function(instance, toast, closedBy){ console.info('closedBy: ' + closedBy); }
                                        }, toast, 'buttonName');
                                    }]
                                    ],
                                    onOpening: function(instance, toast){ console.info('callback abriu!'); },
                                    onClosing: function(instance, toast, closedBy){ console.info('closedBy: ' + closedBy); }
                            });                            
                            
                    break;
                }
        },
        CallAjax: function(url, data, route, method_type){
            var $self       = Student.config, timer, data_object = {},
                $base_host  = $.trim($self.content_wrapper.attr('data-host')),
                $url        =  $base_host + url;

                // console.log($url)
            $.ajax({
                type: method_type,
                url: $url,
                data: data,
                dataType:'json',
                beforeSend: function(){
                    timer && clearTimeout(timer);
                    timer = setTimeout(function()
                    {
                        $("body").addClass("loading"); 
                    },
                    1000);                    

                    switch(route){
                        case 2: 
                                // $self.spinner_border.removeClass('hidden');
                        break;
                    }                    
                },
                complete: function(){
                    clearTimeout(timer);
                    $("body").removeClass("loading"); 

                    switch(route){
                        case 2: 
                                // $self.spinner_border.addClass('hidden');
                        break;
                    }                    
                },                
                success: function(evt){ 
                    console.log(typeof(evt));
                    if(evt){
                        switch(route){
                            case 1: Student.ViewMoreStudents(2, evt); break; 
                            case 2: Student.ViewMoreSchools(2, evt); break; 
                            case 3: Student.SearchForStudents(2, evt); break; 
                            case 4: Student.Logout(2, evt); break; 
                        }    
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log('error: ' + textStatus + ': ' + errorThrown);
                }
            }); 
        }//end sa callajax 
    }
    Student.Init({
            content_wrapper            : $('.content-wrapper'),
            btn_view_more_student      : $('#btn-view-more-student'),
            btn_view_more_school       : $('#btn-view-more-school'),
            modal_student              : $('#modal-student'),
            btn_close_modal            : $('#btn-close-modal'),
            tbl_students               : $('#tbl-students'),
            tbl_schools                : $('#tbl-schools'),
            student_table              : $('#student-table'), 
            school_table               : $('#school-table'), 
            title_school               : $('#title-school'),
            title_student              : $('#title-student'),
            form_search                : $('#form-search'),
            in_search                  : $('#in-search'),
            btn_search                 : $('#btn-search'),
            nav_search                 : $('#nav-search'),
            school_card_body           : $('#school-card-body'),
            a_view_profile             : $('#a-profile'),
            a_edit_profile             : $('#a-edit-profile'),
            logout                     : $('#logout'),
            modal_profile              : $('#modal-profile'),

            in_first_name              : $('#in-first-name'),
            in_middle_name             : $('#in-middle-name'),
            in_last_name               : $('#in-last-name'),
            in_birthday                : $('#in-birthday'),
            in_user_name_login         : $('#in-user-name-login')
    });
});