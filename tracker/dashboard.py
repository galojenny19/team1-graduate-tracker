from django.views.generic import (TemplateView)  
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect

class AdminDashboard(TemplateView):
    template_name='dashboard/admin.html'

class StudentDashboard(TemplateView):
    template_name='dashboard/student.html'

    # def get(request):
    #     print(request.session.items())
    #     return HttpResponse({})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['IsLogin'] = self.request.session['IsLogin']

        return context

    def get(self, request, *args, **kwargs):
        IsLogin = request.session.get('IsLogin', 0)
        print(self.request.session.items())
        print('get_context_data')
        if IsLogin == False:
            return redirect({})        
        else:    
            print(request.session.items())
            return super().get(request, *args, **kwargs) 