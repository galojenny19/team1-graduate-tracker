from rest_framework import serializers
from .models import StudentDetails, Students, Courses, SchoolAdmin, Profiles, Schools, Barangays, Cities

class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Cities
        fields = '__all__'                
        
class BarangaySerializer(serializers.ModelSerializer):
    class Meta:
        model = Barangays
        fields = '__all__'                
        # fields = ['SchoolCode','SchoolName', 'SchoolStreet']
        
class SchoolSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schools
        fields = '__all__'        
        
class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profiles
        fields = '__all__'                
        
        
class SchoolAdminSerializer(serializers.ModelSerializer):
    class Meta:
        model = SchoolAdmin
        fields = '__all__'                
        
class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Courses
        fields = '__all__' 
        
class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Students
        fields = '__all__'                                
        
class StudentDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentDetails
        fields = '__all__'                                        