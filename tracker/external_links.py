from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.conf import settings
from tracker.common import Repeated
import requests


@csrf_exempt
@api_view(['GET', 'PUT', 'POST', ])
def course_transaction(request):
    value = Repeated()
    if request.is_secure():
        url = 'https://' + request.META['HTTP_HOST']
    else:
        url = 'http://' + request.META['HTTP_HOST']

    headers = {'Authorization': 'Token ' + settings.TOKEN}

    if request.method == 'POST':
        url = url + '/api/courses/v2/'
        result = requests.post(url, data=request.data, headers=headers)
    elif request.method == 'PUT':
        if value.key_not_exists("course_id", request.data):
            return Response({"result": "course_id is required"},
                            status=status.HTTP_201_CREATED)

        # course_id = request.data['course_id']
        print(request.method)
        # url = url + F'/api/courses/{course_id}/'
        url = url + '/api/courses/v2/'
        result = requests.put(url, data=request.data, headers=headers)
    else:
        course_name = request.GET.get('course_name', '')
        url = url + F'/api/courses/v2/?course_name={course_name}'
        result = requests.get(url, data=request.data, headers=headers)

    response = HttpResponse(
        content=result.content,
        status=result.status_code,
        content_type=result.headers['Content-Type']
    )

    return response


@api_view(['GET', ])
def student_list(request):
    if request.is_secure():
        url = 'https://' + request.META['HTTP_HOST']
    else:
        url = 'http://' + request.META['HTTP_HOST']

    course_name = request.GET.get('course_name', '')
    student_name = request.GET.get('student_name', '')

    url = url + \
        F'/api/students/v2/?course_name={course_name}&student_name={student_name}'
    headers = {'Authorization': 'Token ' + settings.TOKEN}

    result = requests.get(url, data=request.data, headers=headers)

    response = HttpResponse(
        content=result.content,
        status=result.status_code,
        content_type=result.headers['Content-Type']
    )

    return response
