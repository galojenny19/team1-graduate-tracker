from .models import Courses, Students, StudentDetails
from .serializers import CourseSerializer, StudentSerializer, StudentDetailSerializer
from django.http import HttpResponse, JsonResponse
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated 
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework import status
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.http import Http404
from django.db import IntegrityError
from django.db import transaction
from tracker.common import Repeated 
from tracker.dbs import DB
from django.db import connections, connection                

import requests
import json


class Studentsv2(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)    
    # queryset = Courses.objects.all()
    # serializer_class = CourseSerializer        
    def get(self, request, format=None):
        course_name = request.GET.get('course_name', '')  
        student_name = request.GET.get('student_name', '')  
        
        list_data={}

        queryset = StudentDetails.objects.select_related().filter(course_fk__course_name__icontains=course_name).filter(student_fk__student_name__icontains=student_name)        
        list_data= queryset.values('student_detail_id'
                        ,'student_fk__student_name'
                        ,'student_fk__company_name'    
                        ,'school_year_graduated'    
                        ,'student_fk__employee_status'    
                        ,'course_fk__course_name'
                        ,'school_fk__school_name')
                                                                                                
        return Response(list_data)


@api_view(['GET', ])
def student_list(request):
    if request.is_secure():
        url = 'https://' + request.META['HTTP_HOST']
    else:
        url = 'http://' + request.META['HTTP_HOST']
        
    course_name = request.GET.get('course_name', '')  
    student_name = request.GET.get('student_name', '') 
       
    url = url + F'/api/students/v2/?course_name={course_name}&student_name={student_name}'        
    headers = {'Authorization': 'Token ' + settings.TOKEN}
    
    if request.method == 'POST':
        result = requests.post(url, data=request.data, headers=headers)    
    elif request.method == 'PUT':    
        result = requests.put(url, data=request.data, headers=headers)    
    else:    
        result = requests.get(url, data=request.data, headers=headers)    
    
    response = HttpResponse(
        content=result.content,
        status=result.status_code,
        content_type=result.headers['Content-Type']
    )

    return response
