"""team1_graduate_tracker URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from tracker import login, registration,dashboard

urlpatterns = [
    path('', login.Login.as_view(), name='login'),
    path('login_verify/', login.Verify.as_view()),
    path('logout/', login.logout, name='logout'),
    path('registration/', registration.Registration.as_view(), name='registration'),
    path('dashboard/admin/', dashboard.AdminDashboard.as_view(), name='dashboard/admin/'),
    path('dashboard/student/', dashboard.StudentDashboard.as_view(), name='dashboard/student/'),
    url(r'^backoffice/',include('backoffice.urls',namespace='backoffice')),
    path('api/', include('tracker.urls')),
]
