from django.conf.urls import url
from backoffice import login
app_name = 'backoffice' 

urlpatterns = [
    url(r'^login/$',login.Login.as_view(),name='login'),
]