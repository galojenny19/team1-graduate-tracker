## Team 1 Graduate Tracker

### General Description
This project allows universities or schools to keep track of students and their professional careers.This will help univerisities and schools as proof that they have yield more professionals than the previous years.

## API Links

#### To transact record on Province module
* Provinces (List / Create / Update)
    * http://127.0.0.1:8002/api/province_transaction/
    
#### To transact record on City module
* Cities (List / Create / Update)
    * http://127.0.0.1:8002/api/city_transaction/

#### To transact record on Barangay module
* Barangays (List / Create / Update)
    * http://127.0.0.1:8002/api/barangay_transaction/

#### To transact record on School module    
* Schools (List / Create / Update)
    * http://127.0.0.1:8002/api/school_transaction/

#### To transact record on Course module
* Courses (List / Create / Update)
    * http://127.0.0.1:8002/api/course_transaction/        

#### School Admin Registration
* Profiles (Create)
    * http://127.0.0.1:8002/api/profile_transaction/

#### Student Registration    
* Students (Create)
    * http://127.0.0.1:8002/api/student_transaction/            

#### Login verification for student and school admin    
* School admins / Students (just pass username and password keys)
    * http://127.0.0.1:8002/api/login_verify/

#### Student List Reports
* Student Lists (to search course and student name, just pass parameter course_name and student_name on method GET)
    * http://127.0.0.1:8002/api/student_list/                    
